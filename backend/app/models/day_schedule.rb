class DaySchedule < ApplicationRecord
  belongs_to :week_schedule
  has_many :work_availabilities
end
