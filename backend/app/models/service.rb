class Service < ApplicationRecord
  has_many :week_schedules
  has_many :work_availabilities

  validates :name, presence: true
end
