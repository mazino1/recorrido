class User < ApplicationRecord
  has_many :work_availabilities

  validates :name, presence: true
  validates :color, presence: true
end
