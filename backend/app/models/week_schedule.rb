class WeekSchedule < ApplicationRecord
  has_one :service
  has_many :day_schedules
  has_many :work_availabilities
end
