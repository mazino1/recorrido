class WorkAvailability < ApplicationRecord
  belongs_to :service
  belongs_to :week_schedule
  belongs_to :day_schedule
  belongs_to :user
end