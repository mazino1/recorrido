class CreateWeekSchedule < ActiveRecord::Migration[6.0]
  def change
    create_table :week_schedules do |t|
      t.references :service, null: false, foreign_key: true
      t.integer :day_of_week
    end
  end
end
