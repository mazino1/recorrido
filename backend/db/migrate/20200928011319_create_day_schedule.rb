class CreateDaySchedule < ActiveRecord::Migration[6.0]
  def change
    create_table :day_schedules do |t|
      t.references :week_schedule, null: false, foreign_key: true
      t.integer :hour_of_day
    end
  end
end
