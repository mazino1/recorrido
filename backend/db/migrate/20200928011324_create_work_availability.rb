class CreateWorkAvailability < ActiveRecord::Migration[6.0]
  def change
    create_table :work_availabilities do |t|
      t.references :service, null: false, foreign_key: true
      t.references :week_schedule, null: false, foreign_key: true
      t.references :day_schedule, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :week_of_year
      t.boolean :available
    end
  end
end
