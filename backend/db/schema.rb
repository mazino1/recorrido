# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_09_28_011324) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "day_schedules", force: :cascade do |t|
    t.bigint "week_schedule_id", null: false
    t.integer "hour_of_day"
    t.index ["week_schedule_id"], name: "index_day_schedules_on_week_schedule_id"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "color"
  end

  create_table "week_schedules", force: :cascade do |t|
    t.bigint "service_id", null: false
    t.integer "day_of_week"
    t.index ["service_id"], name: "index_week_schedules_on_service_id"
  end

  create_table "work_availabilities", force: :cascade do |t|
    t.bigint "service_id", null: false
    t.bigint "week_schedule_id", null: false
    t.bigint "day_schedule_id", null: false
    t.bigint "user_id", null: false
    t.integer "week_of_year"
    t.boolean "available"
    t.index ["day_schedule_id"], name: "index_work_availabilities_on_day_schedule_id"
    t.index ["service_id"], name: "index_work_availabilities_on_service_id"
    t.index ["user_id"], name: "index_work_availabilities_on_user_id"
    t.index ["week_schedule_id"], name: "index_work_availabilities_on_week_schedule_id"
  end

  add_foreign_key "day_schedules", "week_schedules"
  add_foreign_key "week_schedules", "services"
  add_foreign_key "work_availabilities", "day_schedules"
  add_foreign_key "work_availabilities", "services"
  add_foreign_key "work_availabilities", "users"
  add_foreign_key "work_availabilities", "week_schedules"
end
