# frozen_string_literal: true

service = Service.find_by(name: 'Recorrido.cl')
week = [19, 20, 21, 22, 23]
weekend = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]

service.week_schedules.each do |week_schedule|
  hour_of_days = week
  hour_of_days = weekend if week_schedule.day_of_week.zero? || week_schedule.day_of_week == 6

  hour_of_days.each do |hour_of_day|
    DaySchedule.find_or_create_by(week_schedule_id: week_schedule.id, hour_of_day: hour_of_day)
  end
end
