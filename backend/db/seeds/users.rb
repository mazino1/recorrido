# frozen_string_literal: true

User.find_or_create_by(name: 'Benjamin', color: 'blue')
User.find_or_create_by(name: 'Bárbara', color: 'purple')
User.find_or_create_by(name: 'Ernesto', color: 'orange')
