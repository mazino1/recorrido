# frozen_string_literal: true

Service.all.each do |service|
  7.times.each { |day_of_week| WeekSchedule.find_or_create_by(service_id: service.id, day_of_week: day_of_week) }
end
